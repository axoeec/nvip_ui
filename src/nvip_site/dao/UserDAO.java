package nvip_site.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import org.apache.commons.codec.binary.Hex;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;

import nvip_site.data.DBConnect;
import nvip_site.model.User;

public class UserDAO {
	private static String dbType = DBConnect.getDatabaseType();
	
	public static byte[] hashPassword( final char[] password, final byte[] salt, final int iterations, final int keyLength ) {

        try {
            SecretKeyFactory skf = SecretKeyFactory.getInstance( "PBKDF2WithHmacSHA512" );
            PBEKeySpec spec = new PBEKeySpec( password, salt, iterations, keyLength );
            SecretKey key = skf.generateSecret( spec );
            byte[] res = key.getEncoded( );
            return res;
        } catch ( NoSuchAlgorithmException | InvalidKeySpecException e ) {
            throw new RuntimeException( e );
        }
    }
	
	/* s must be an even-length string. */
	public static byte[] hexStringToByteArray(String s) {
	    int len = s.length();
	    byte[] data = new byte[len / 2];
	    for (int i = 0; i < len; i += 2) {
	        data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
	                             + Character.digit(s.charAt(i+1), 16));
	    }
	    return data;
	}
	
	private static boolean checkUserExistance(Connection conn, String userName) {
		boolean userExist = false;
		try(PreparedStatement stmt = conn.prepareStatement("SELECT COUNT(u.user_id) AS userCount, user_id FROM User u " + 
					" WHERE u.user_name = ?")) {
			
			stmt.setString(1, userName);
			
			ResultSet rs = stmt.executeQuery();
			
			while(rs.next()) {
				int userCount = rs.getInt("userCount");
				if (userCount >= 1) {
					rs.close();
					return true;
				}
				else {
					rs.close();
					return false;
				}
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 		
		
		return userExist;
	}
	
	public static boolean checkUserExistance(String userName) {
		boolean userExist = false;
		try(Connection conn = DBConnect.getConnection()){
			conn.setAutoCommit(false);	
			userExist = checkUserExistance(conn, userName);
			conn.commit();

			return userExist;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 		
		
		return userExist;
	}
	
	private static int createUser(Connection conn, User user, String passwordHash) {
		try(PreparedStatement stmt = conn.prepareStatement("INSERT user SET user_name=?, password_hash=?, first_name = ?, "
					+ "last_name=?, email=?, role_id=?, registered_date=?;")) {

			stmt.setString(1, user.getUserName());
			stmt.setString(2, passwordHash);
			stmt.setString(3, user.getFirstName());
			stmt.setString(4, user.getLastName());
			stmt.setString(5, user.getEmail());
			stmt.setInt(6, user.getRoleId());
			stmt.setTimestamp(7, Timestamp.valueOf(LocalDateTime.now()));
			
			
			int rs = stmt.executeUpdate();
			
			return rs;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 		
		
		return -1;
	}
		
	private static int updateToken(Connection conn, String userName, String token, LocalDateTime loginDate, LocalDateTime expirationDate) {
		try (PreparedStatement stmt = conn.prepareStatement("UPDATE user SET token = ?, token_expiration_date = ?, " + 
					"last_login_date = ? WHERE user_name = ?;")){

			stmt.setString(1, token);
			stmt.setTimestamp(2, Timestamp.valueOf(loginDate));
			stmt.setTimestamp(3, Timestamp.valueOf(expirationDate));
			stmt.setString(4, userName);		
			
			int rs = stmt.executeUpdate();
			
			return rs;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 		
		
		return -1;
	}
	
	public static int updateToken(String userName, String token, LocalDateTime loginDate, LocalDateTime expirationDate) {
		try(Connection conn = DBConnect.getConnection()){
			conn.setAutoCommit(false);
			int rs = updateToken(conn, userName, token, loginDate, expirationDate);
			conn.commit();
			conn.close();
			return rs;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 		
		
		return -1;
	}
	
	private static User getRoleIDandExpirationDate(Connection conn, String userName, String token) {
		// Password Hashing Logic
		
		try(PreparedStatement stmt = conn.prepareStatement("SELECT COUNT(u.user_id) AS userCount, user_id, role_id, token_expiration_date FROM User u " + 
					" WHERE u.user_name = ? AND token = ?")){		
			
			stmt.setString(1, userName);
			stmt.setString(2, token);
			
			ResultSet rs = stmt.executeQuery();
						
			while(rs.next()) {
				int userCount = rs.getInt("userCount");
				if (userCount == 1) {
					int roleID = rs.getInt("role_id");
					int userID = rs.getInt("user_id");
					LocalDateTime expirationDate= rs.getTimestamp("token_expiration_date").toLocalDateTime();
					User userInDB = new User(userID, null, userName, null, null, null, null, roleID, expirationDate);
					rs.close();
					return userInDB;
				}
				else {
					rs.close();
					return null;
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	
		return null;
	}
	
	public static User getRoleIDandExpirationDate(String userName, String token) {
		
		try(Connection conn = DBConnect.getConnection()){		
			
			User user = getRoleIDandExpirationDate(conn, userName, token);
			return user;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	
		return null;
	}
	
	private static User login(Connection conn, String userName) {
		// Password Hashing Logic
		
		try(PreparedStatement stmt = conn.prepareStatement("SELECT COUNT(u.user_id) AS userCount, user_id, password_hash, role_id, first_name, last_name FROM User u " + 
					" WHERE u.user_name = ?")){		
			stmt.setString(1, userName);
			
			ResultSet rs = stmt.executeQuery();
						
			while(rs.next()) {
				int userCount = rs.getInt("userCount");
				if (userCount == 1) {
					int roleID = rs.getInt("role_id");
					String firstName = rs.getString("first_name");
					String lastName = rs.getString("last_name");
					String passwordHash = rs.getString("password_hash");
					int userID = rs.getInt("user_id");
					User userInDB = new User(userID, null, userName, firstName, lastName, null, passwordHash, roleID, null);
					rs.close();
					return userInDB;
				}
				else {
					rs.close();
					return null;
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
	
		return null;
	}
	
	public static User login(String userName, String password) {
		// Password Hashing Logic

		try(Connection conn = DBConnect.getConnection()){
			
			User user = login(conn, userName.toLowerCase());
			if (user==null) {
				conn.close();
				return null;
			}
			
			int iterations = 10000;
	        int keyLength = 512;
	        char[] passwordChars = password.toCharArray();
	        
	        String dbHash = user.getPasswordHash().substring(0, keyLength/4);
	        String salt = user.getPasswordHash().substring(keyLength/4);
	        byte[] saltBytes = UserDAO.hexStringToByteArray(salt);
	        
	        byte[] hashedBytes = hashPassword(passwordChars, saltBytes, iterations, keyLength);
	        String hashedString = Hex.encodeHexString(hashedBytes);
	        
	        user.setPasswordHash(null);
	        
	        if(!hashedString.equalsIgnoreCase(dbHash)) {
	        	conn.close();
	        	return null;
	        }
	        
			conn.setAutoCommit(false);
			
			SecureRandom random = new SecureRandom();
		    byte tokenBytes[] = new byte[64];
		    random.nextBytes(tokenBytes);
		    String tokenString = Hex.encodeHexString(tokenBytes);
		    
		    LocalDateTime loginDate = LocalDateTime.now();
		    LocalDateTime expirationDate = null;
		    if (user.getRoleId()==1) {
		    	expirationDate = LocalDateTime.now().plusHours(3);
		    }
		    else if(user.getRoleId()==2) {
		    	expirationDate = LocalDateTime.now().plusDays(5);
		    }
		    
		    int rs = updateToken(conn, userName, tokenString, loginDate, expirationDate);
		    conn.commit();
			conn.close();
		    
		    user.setToken(tokenString);
		    user.setExpirationDate(expirationDate);
			
		    return user;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	
		
		return null;
	}
	
	public static int createUser(User user, String password) {
		try(Connection conn = DBConnect.getConnection()){
			boolean userExist = checkUserExistance(conn, user.getUserName());
			
			if(userExist) {
				return -2;
			}
			
			SecureRandom random = new SecureRandom();
		    byte saltBytes[] = new byte[64];
		    random.nextBytes(saltBytes);
		    
		    String salt = Hex.encodeHexString(saltBytes);
	        int iterations = 10000;
	        int keyLength = 512;
	        char[] passwordChars = password.toCharArray();

	        byte[] hashedBytes = UserDAO.hashPassword(passwordChars, saltBytes, iterations, keyLength);
	        String hashedPassword = Hex.encodeHexString(hashedBytes);
	        hashedPassword = hashedPassword + salt;
			
			conn.setAutoCommit(false);
			int rs = createUser(conn, user, hashedPassword);
			conn.commit();
			conn.close();
			return rs;
			
			
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		return -1;
	}
}