package nvip_site.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import nvip_site.data.DBConnect;

public class MainDAO {
	private static String dbType = DBConnect.getDatabaseType();
	private static final String[] MAIN_PAGE_COUNTS = {"totalCveCount", "avgCvesAdded", "avgCvesUpdated", "avgTimeGapNvd"};
	
	public static Map<String, Integer> getMainPageCounts() {
		// Password Hashing Logic
		
		if(dbType == "MySQL") {
			try(Connection conn = DBConnect.getConnection()){
				CallableStatement stmt = conn.prepareCall("CALL getMainPageCounts()");

				ResultSet rs = stmt.executeQuery();
				
				Map<String, Integer> mainPageCounts = new HashMap<>();
				
				while(rs.next()) {
					mainPageCounts.put(MAIN_PAGE_COUNTS[0], rs.getInt("totalCveCount"));
					mainPageCounts.put(MAIN_PAGE_COUNTS[1], rs.getInt("avgCvesAdded"));
					mainPageCounts.put(MAIN_PAGE_COUNTS[2], rs.getInt("avgCvesUpdated"));
					mainPageCounts.put(MAIN_PAGE_COUNTS[3], rs.getInt("avgTimeGapNvd"));
				}
				conn.close();
				
				return mainPageCounts;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		}
		
		return null;
	}
}