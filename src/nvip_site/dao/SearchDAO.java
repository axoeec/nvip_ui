package nvip_site.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import nvip_site.model.CvssScore;
import nvip_site.dao.util.VulnerabilityUtil;
import nvip_site.data.DBConnect;
import nvip_site.model.Product;
import nvip_site.model.VdoCharacteristic;
import nvip_site.model.Vulnerability;
import nvip_site.servlet.VulnerabilityServlet;

public class SearchDAO {
	private static final Logger logger = LogManager.getLogger(SearchDAO.class);
	private static String dbType = DBConnect.getDatabaseType();
	private final static int defaultLimit = 10000;

	/**
	 * Parses the information retrieved from the Search Form initialization query
	 * and splits the returned strings into arrays.
	 * 
	 * @param infoType   Type of form info that is being returned (i.e. CVSS Scores,
	 *                   VDO labels)
	 * @param infoArrStr Delimited string containing all the values needed to
	 *                   initialize the Search Form for the given info type
	 * @return Map with the info type as the key and an array containing all the
	 *         entities for the given info type
	 */
	private static HashMap<String, String[]> parseSearchInfo(String infoType, String infoArrStr) {
		HashMap<String, String[]> infoMap = new HashMap<>();

		if (infoType == "cvssScores") {
			infoMap.put(infoType, infoArrStr.split(";"));
		} else if (infoType == "vdoNounGroups") {
			String[] vdoEntities = infoArrStr.split("\\|");

			for (String vdoEntity : vdoEntities) {
				String[] vdo = vdoEntity.split(":");
				infoMap.put(vdo[0], vdo[1].split(";"));
			}
		}

		return infoMap;
	}

	/**
	 * Calls a stored procedure containing labels for parameters that can be
	 * searched in the search form (i.e. VDO Noun Groups, VDO Labels, CVSS Score
	 * labels, etc.)
	 * 
	 * @return Map containing parameter name of labels (i.e. CVSS Scores) and the
	 *         label strings
	 */
	public static Map<String, Map<String, String[]>> getSearchInfo() {
		try (Connection conn = DBConnect.getConnection()) {
			Map<String, Map<String, String[]>> searchMap = new HashMap<>();

			CallableStatement stmt = conn.prepareCall("CALL getSearchFormInfo()");

			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				searchMap = Stream.of(new String[][] { { "cvssScores", rs.getString("cvss_scores") }, { "vdoNounGroups", rs.getString("vdo_noun_groups") } })
						.collect(Collectors.toMap(data -> data[0], data -> parseSearchInfo(data[0], data[1])));
			}

			return searchMap;
		} catch (SQLException e) {
			logger.error(e.toString());
		}

		return null;
	}

	/**
	 * Calls a stored procedure to obtain vulnerabilities that match the parameters
	 * passed in. Returns the set of vulnerabilities ordered by earliest update.
	 * through the search form.
	 * 
	 * @param vulnId        - Vulnerability id. Used to define range of results
	 *                      (above or below given id)
	 * @param keyword       - Keyword that is within the vulnerability description
	 * @param startDate     - Starting date for last update of the vulnerabilities
	 * @param endDate       - End date for last update of the vulnerabilities
	 * @param cvssScores    - Set of CVSS scores values which the vulnerabilities
	 *                      must contain (at least one)
	 * @param vdoNounGroups - Set of VDO Noun Groups which the vulnerabilities must
	 *                      contain (at least one)
	 * @param vdoLabels     - Set of VDO labels which the vulnerabilities must
	 *                      contain (at least one)
	 * @param inMitre       - Parameter which indicates vulnerability has an entry
	 *                      on MITRE
	 * @param inNvd         - Parameter which indicates vulnerability has an entry
	 *                      on NVD
	 * @param limitCount    - Sets the limit of vulnerabilities returned. Defaults
	 *                      to the set default if not provided
	 * @param isBefore      - Defines if search range before or after given id
	 * @return Map of a list of vulnerabilities to the total count of those
	 *         vulnerabilities
	 */
	public static Map<Integer, List<Vulnerability>> getSearchResults(int vulnId, String keyword, LocalDate startDate, LocalDate endDate, String cvssScores, String[] vdoNounGroups, String[] vdoLabels, String inMitre, String inNvd,
			int limitCount, boolean isBefore) {

		try (Connection conn = DBConnect.getConnection()) {
			Timestamp fixedDate = null;
			String[] sources = {};
			VdoCharacteristic[] vdoList = {};
			CvssScore[] cvssScoreList = null;
			Product[] products = null;
			Product product = null; // Temporary product until update Vuln entity to handle multiple products
			List<Vulnerability> searchResults = new ArrayList<>();
			int totalCount = 0;

			HashMap<Integer, List<Vulnerability>> searchResultMap = new HashMap<Integer, List<Vulnerability>>();

			CallableStatement stmt = conn.prepareCall("CALL getVulnerabilitiesByCriteria(?,?,?,?,?,?,?,?,?,?,?)");
			stmt.setInt("vuln_id", vulnId);
			stmt.setString("keyword", keyword);
			stmt.setTimestamp("start_date", startDate == null ? null : Timestamp.valueOf(startDate.atStartOfDay()));
			stmt.setTimestamp("end_date", endDate == null ? null : Timestamp.valueOf(endDate.atTime(LocalTime.MAX)));
			stmt.setString("cvss_scores", cvssScores);
			stmt.setString("vdo_noun_groups", vdoNounGroups == null ? null : String.join(",", vdoNounGroups));
			stmt.setString("vdo_labels", vdoLabels == null ? null : String.join(",", vdoLabels));
			stmt.setString("in_mitre", (inMitre != null && !inMitre.isEmpty()) ? (inMitre.equals("true") ? "1" : "0") : null);
			stmt.setString("in_nvd", (inNvd != null && !inNvd.isEmpty()) ? (inNvd.equals("true") ? "1" : "0") : null);
			stmt.setInt("limit_count", limitCount == 0 ? defaultLimit : limitCount);
			stmt.setBoolean("is_before", isBefore);

			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				vdoList = VulnerabilityUtil.parseVDOList(rs.getString("cve_id"), rs.getString("vdo_labels"), rs.getString("vdo_label_confidences"), rs.getString("vdo_noun_groups"), ";", null);
				cvssScoreList = VulnerabilityUtil.parseCvssScoreList(rs.getString("cve_id"), rs.getString("base_severities"), rs.getString("severity_confidences"), rs.getString("impact_scores"), rs.getString("impact_confidences"), ";",
						null);
				products = VulnerabilityUtil.parseProductList(rs.getString("product_ids"), rs.getString("cpe_list"), rs.getString("domains"), rs.getString("versions"), ";", null);

				// Temporary product until update Vuln entity to handle multiple products
				//product = products.length > 0 ? products[0] : null;

				searchResults.add(new Vulnerability(rs.getInt("vuln_id"), rs.getString("cve_id"), rs.getString("description"), rs.getString("platform"), rs.getString("published_date"), rs.getString("last_modified_date"),
						(fixedDate == null ? null : fixedDate.toLocalDateTime()), rs.getBoolean("exists_at_mitre"), rs.getBoolean("exists_at_nvd"), sources, vdoList, cvssScoreList, products));

				totalCount = rs.getInt("total_count");
			}

			conn.close();

			searchResultMap.put(totalCount, searchResults);
			return searchResultMap;
		} catch (SQLException e) {
			logger.error(e.toString());
		}

		return null;
	}

	public static void main(String[] args) {

	}
}