package nvip_site.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import nvip_site.model.CVSSupdate;
import nvip_site.model.CvssScore;
import nvip_site.dao.util.VulnerabilityUtil;
import nvip_site.data.DBConnect;
import nvip_site.model.Product;
import nvip_site.model.VDOgroup;
import nvip_site.model.VDOupdateInfo;
import nvip_site.model.VdoCharacteristic;
import nvip_site.model.Vulnerability;
import nvip_site.model.VulnerabilityDetails;
import nvip_site.model.VulnerabilityDomain;
import nvip_site.model.VulnerabilityForReviewList;

public class ReviewDAO {
	private static String dbType = DBConnect.getDatabaseType();
	private final static int defaultLimit = 10000; 
	
	/**
	 * Parses the information retrieved from the Search Form initialization query 
	 * and splits the returned strings into arrays.
	 * @param infoType Type of form info that is being returned (i.e. CVSS Scores, VDO labels)
	 * @param infoArrStr Delimited string containing all the values needed to initialize the 
	 * Search Form for the given info type
	 * @return Map with the info type as the key and an array containing all the entities 
	 * for the given info type
	 */
	private static HashMap<String, String[]> parseSearchInfo(String infoType, String infoArrStr){
		HashMap<String, String[]> infoMap = new HashMap<>();
		
		if(infoType == "cvssScores") {
			infoMap.put(infoType, infoArrStr.split(";"));
		}
		else if (infoType == "vdoNounGroups") {
			String[] vdoEntities = infoArrStr.split("\\|");
			
			for(String vdoEntity : vdoEntities) {
				String[] vdo = vdoEntity.split(":");
				infoMap.put(vdo[0], vdo[1].split(";"));
			}
		}
		
		return infoMap;
	}
	
	/**
	 * Calls a stored procedure containing labels for parameters that can be searched in the search form
	 * (i.e. VDO Noun Groups, VDO Labels, CVSS Score labels, etc.)
	 * @return Map containing parameter name of labels (i.e. CVSS Scores) and the label strings
	 */
	public static Map<String, Map<String, String[]>> getSearchInfo(){
		if(dbType == "MySQL") {
			try(Connection conn = DBConnect.getConnection()){
				Map<String, Map<String, String[]>> searchMap = new HashMap<>();
				
				CallableStatement stmt = conn.prepareCall("CALL getSearchFormInfo()");
				
				ResultSet rs = stmt.executeQuery();
				
				while(rs.next()) {

					searchMap = Stream.of(new String[][] {
						{"cvssScores", rs.getString("cvss_scores")},
						{"vdoNounGroups", rs.getString("vdo_noun_groups")}
						}).collect(Collectors.toMap(data -> data[0], data -> parseSearchInfo(data[0], data[1])));
				}
				conn.close();
				return searchMap;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		}
		
		return null;
	}
	
	/**
	 * Calls a stored procedure to obtain vulnerabilities that match the parameters passed in.
	 * Returns the set of vulnerabilities ordered by earliest update.
	 * through the search form.
	 * @param vulnId - Vulnerability id. Used to define range of results (above or below given id)
	 * @param keyword - Keyword that is within the vulnerability description
	 * @param startDate - Starting date for last update of the vulnerabilities
	 * @param endDate - End date for last update of the vulnerabilities
	 * @param cvssScores - Set of CVSS scores values which the vulnerabilities must contain (at least one)
	 * @param vdoNounGroups - Set of VDO Noun Groups which the vulnerabilities must contain (at least one)
	 * @param vdoLabels - Set of VDO labels which the vulnerabilities must contain (at least one)
	 * @param inMitre - Parameter which indicates vulnerability has an entry on MITRE
	 * @param inNvd - Parameter which indicates vulnerability has an entry on NVD
	 * @param limitCount - Sets the limit of vulnerabilities returned. Defaults to the set default if not provided
	 * @param isBefore - Defines if search range before or after given id
	 * @return Map of a list of vulnerabilities to the total count of those vulnerabilities
	 */
	public static List<VulnerabilityForReviewList> getSearchResults(LocalDate searchDate, boolean crawled, boolean rejected, boolean accepted, boolean reviewed) {
		try (Connection conn = DBConnect.getConnection()) {

			// Get the CVEs for the last 3 days from 2 days ago to today (inclusive)
			LocalDateTime today = LocalDateTime.of(searchDate, LocalTime.MIDNIGHT);
			
			List<VulnerabilityForReviewList> dailyVulns = new ArrayList<VulnerabilityForReviewList>();
			
			PreparedStatement stmt;
			ResultSet rs;
			
			String query = "SELECT v.vuln_id, v.cve_id, v.platform, v.last_modified_date, v.exists_at_nvd, v.exists_at_mitre, v.status_id, drh.run_date_time " + 
					"from vulnerability as v inner join vulnerabilityupdate as vu on v.vuln_id=vu.vuln_id " + 
					"inner join dailyrunhistory as drh on vu.run_id = drh.run_id " + 
					"where drh.run_date_time BETWEEN ? AND ? and (";
			
			if (!crawled && !rejected && !accepted && !reviewed) {
				return null;
			}
			boolean orStatement = false;
			if (crawled) {
				query = query + "v.status_id = 1"; 
				orStatement = true;
			}
			
			if (rejected) {
				if (orStatement) {
					query = query + " or ";
				}
				query = query + "v.status_id = 2"; 
				orStatement = true;
			}
			
			if (reviewed) {
				if (orStatement) {
					query = query + " or ";
				}
				
				query = query + "v.status_id = 3"; 
				orStatement = true;
			}
			
			if (accepted) {
				if (orStatement) {
					query = query + " or ";
				}
				
				query = query + "v.status_id = 4"; 
			}
			
			query = query + ");";
			
			stmt = conn.prepareStatement(query);
			stmt.setTimestamp(1, Timestamp.valueOf(today));
			stmt.setTimestamp(2, Timestamp.valueOf(today.plusDays(1)));
			
			rs = stmt.executeQuery();
			
			while (rs.next()) {
				String vuln_id = rs.getString("vuln_id");
				String cve_id = rs.getString("cve_id");
				String status_id = rs.getString("status_id");
				String run_date_time = rs.getString("run_date_time");
//				String description = rs.getString("description");
				dailyVulns.add(new VulnerabilityForReviewList(vuln_id, cve_id, status_id, null, run_date_time));

			}
			
			conn.close();
			
			return dailyVulns;
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}

		return null;
	}
	
	public static VulnerabilityDetails getVulnerabilityDetails(String cveID) {
		try (Connection conn = DBConnect.getConnection()) {

			
			VulnerabilityDetails vulnDetails = null;
			
			PreparedStatement stmt;
			ResultSet rs;
			
			stmt = conn.prepareStatement("SELECT v.vuln_id, v.cve_id, v.description, p.product_id, p.domain, p.cpe, ar.release_date, ar.version, vng.vdo_noun_group_id, vng.vdo_noun_group_name, vl.vdo_label_id, vl.vdo_label_name, vc.vdo_confidence, " + 
					"cs.cvss_severity_id, cs.cvss_severity_class, c.impact_score, v.status_id, u.first_name, u.last_name, u.user_name, uvu.user_id, uvu.update_date " + 
					"from vulnerability as v inner join vdocharacteristic as vc on v.cve_id=vc.cve_id " + 
					"left join affectedrelease ar on v.cve_id=ar.cve_id " + 
					"left join Product p on ar.product_id=p.product_id " + 
					"left join (select user_id, cve_id, max(datetime) as update_date from uservulnerabilityupdate group by user_id,cve_id) uvu on v.cve_id = uvu.cve_id " + 
					"left join user u on u.user_id=uvu.user_id " + 
					"inner join vdonoungroup as vng on vc.vdo_noun_group_id = vng.vdo_noun_group_id " + 
					"inner join vdolabel as vl on vc.vdo_label_id=vl.vdo_label_id " + 
					"inner join cvssscore as c on v.cve_id=c.cve_id " + 
					"inner join cvssseverity as cs on c.cvss_severity_id=cs.cvss_severity_id " + 
					"where v.cve_id = ?");
			
			stmt.setString(1, cveID);
			rs = stmt.executeQuery();
			
			while (rs.next()) {
				
				if (vulnDetails==null) {
					String vuln_id = rs.getString("vuln_id");
					String cve_id = rs.getString("cve_id");
					String status_id = rs.getString("status_id");
					String description = rs.getString("description");
					String cvssScore = rs.getString("cvss_severity_class");
					String impactScore = rs.getString("impact_score");
					String userFname = rs.getString("first_name");
					String userLname = rs.getString("last_name");
					String userID = rs.getString("user_id");
					String userName = rs.getString("user_name");
					String updateDate = rs.getString("update_date");
					
											
					vulnDetails = new VulnerabilityDetails(vuln_id, cve_id, description, status_id, cvssScore, impactScore, userFname, userLname, userName, userID, updateDate);
				}
				
				String vdoGroup = rs.getString("vdo_noun_group_name");
				String vdoLabel = rs.getString("vdo_label_name");
				String vdoConf = rs.getString("vdo_confidence");
				
				if (vulnDetails.getVdoGroups().containsKey(vdoGroup)) {
					vulnDetails.getVdoGroups().get(vdoGroup).getVdoLabel().put(vdoLabel, vdoConf);
				}
				else {
					vulnDetails.getVdoGroups().put(vdoGroup, new VDOgroup(vdoGroup, vdoLabel, vdoConf));
				}
				
				String domain = rs.getString("domain");
				String cpe = rs.getString("cpe");
				String version = rs.getString("version");
				String product_id = rs.getString("product_id");
				
				if (domain!=null || cpe!=null) {
					vulnDetails.getVulnDomain().add(new VulnerabilityDomain(product_id, domain, cpe, version));
				}
			}
			
			conn.close();
			
			return vulnDetails;
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}

		return null;
	}
	
	public static int updateDailyVulnerability(int dateRange) {
		try (Connection conn = DBConnect.getConnection()) {
			
			CallableStatement stmt = conn.prepareCall("CALL prepareDailyVulnerabilities(?, ?, ?)");
			
			LocalDateTime today = LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT).plusDays(1);

			stmt.setTimestamp(1, Timestamp.valueOf(today.minusDays(dateRange)));
			stmt.setTimestamp(2, Timestamp.valueOf(today));
			
			stmt.registerOutParameter("cveCount", Types.INTEGER);
	
			stmt.execute();
			
			conn.close();
			
			return stmt.getInt("cveCount");
								
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
		
		return -1;

	}
	
	public static int atomicUpdateVulnerability(int status_id, int vuln_id, int userID, String cve_id, String info) {
		try (Connection conn = DBConnect.getConnection()) {
			
			conn.setAutoCommit(false);
			
			int rs = atomicUpdateVulnerability(conn, status_id, vuln_id, userID, cve_id, info);
			conn.commit();
			conn.close();
			return rs;
								
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
		
		return -1;

	}
	
	public static int updateVulnerabilityDescription(Connection conn, String description, int vuln_id) {
		try {
			
			PreparedStatement stmt;
			int rs;
						
			stmt = conn.prepareStatement("UPDATE vulnerability SET description = ? WHERE vuln_id=?;");
			
			stmt.setString(1, description);
			stmt.setInt(2, vuln_id);
			rs = stmt.executeUpdate();
			
//			System.out.println("Response: " + Integer.toString(rs));
			return rs;
								
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
		
		return -1;

	}
	
	public static int atomicUpdateVulnerability(Connection conn, int status_id, int vuln_id, int userID, String cve_id, String info) {
		try {
						
			PreparedStatement stmt;
			int rs;
			
			LocalDateTime today = LocalDateTime.now();
			
			stmt = conn.prepareStatement("UPDATE vulnerability SET last_modified_date= ?, status_id=? WHERE vuln_id=?;");
			
			stmt.setTimestamp(1, Timestamp.valueOf(today));
			stmt.setInt(2, status_id);
			stmt.setInt(3, vuln_id);
			
			rs = stmt.executeUpdate();
						
			stmt = conn.prepareStatement("INSERT INTO nvip.uservulnerabilityupdate (user_id, cve_id, datetime, info) VALUES (?, ?, ?, ?);");
			
			stmt.setInt(1, userID);
			stmt.setString(2, cve_id);
			stmt.setTimestamp(3, Timestamp.valueOf(today));
			stmt.setString(4, info);
			
			rs = stmt.executeUpdate();
						
			return rs;
								
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
		
		return -1;

	}
	
	public static int updateVulnerabilityCVSS(Connection conn, CVSSupdate cvssUpdate, String cve_id) {
		try {
			
			PreparedStatement stmt;
			int rs;
					
			stmt = conn.prepareStatement("DELETE FROM CvssScore WHERE cve_id=?;");
			stmt.setString(1, cve_id);
			rs = stmt.executeUpdate();
			
			stmt = conn.prepareStatement("INSERT INTO CvssScore (cve_id, cvss_severity_id, severity_confidence, impact_score, impact_confidence) VALUES (?,?,?,?,?);");
			stmt.setString(1, cve_id);
			stmt.setInt(2, cvssUpdate.getCvss_severity_id());
			stmt.setDouble(3, cvssUpdate.getSeverity_confidence());
			stmt.setDouble(4, cvssUpdate.getImpact_score());
			stmt.setDouble(5, cvssUpdate.getImpact_confidence());
			rs = stmt.executeUpdate();
			
			return rs;
								
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
		
		return -1;

	}
	
	public static int updateVulnerabilityVDO(Connection conn, VDOupdateInfo vdoUpdate, String cve_id) {
		try {
			
			PreparedStatement stmt;
			int rs;
			
			stmt = conn.prepareStatement("DELETE FROM VdoCharacteristic WHERE cve_id=?;");
			stmt.setString(1, cve_id);
			rs = stmt.executeUpdate();
			
			for (int i=0; i<vdoUpdate.getVdoRecords().size(); i++) {
				stmt = conn.prepareStatement("INSERT INTO VdoCharacteristic (cve_id, vdo_label_id,vdo_confidence,vdo_noun_group_id) VALUES (?,?,?,?);");
				stmt.setString(1, cve_id);
				stmt.setInt(2, vdoUpdate.getVdoRecords().get(i).getLabelID());
				stmt.setDouble(3, vdoUpdate.getVdoRecords().get(i).getConfidence());
				stmt.setInt(4, vdoUpdate.getVdoRecords().get(i).getGroupID());
				rs = stmt.executeUpdate();
			}
			
			return rs;
								
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
		
		return -1;

	}
	
	public static int removeProductsFromVulnerability(Connection conn, int[] productsID, String cve_id) {
		try {
			
			PreparedStatement stmt;
			int rs=0;
			
			for (int i=0; i<productsID.length; i++) {
				stmt = conn.prepareStatement("DELETE FROM AffectedRelease where product_id = ?  AND cve_id = ?;");
				stmt.setInt(1, productsID[i]);
				stmt.setString(2, cve_id);
				rs = stmt.executeUpdate();
			}
			
			return rs;
								
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
		
		return -1;

	}
	
	public static int complexUpdate(boolean updateDescription, boolean updateVDO, boolean updateCVSS, boolean updateAffRel, 
			int status_id, int vuln_id, int userID, String cve_id, String updateInfo,
			String cveDescription, VDOupdateInfo vdoUpdate, CVSSupdate cvssUpdate, int[] productsToRemove) {
		
		try {
			Connection conn = DBConnect.getConnection();
			conn.setAutoCommit(false);
			
			int rs = 0;
			
			if(updateDescription) {
				rs = updateVulnerabilityDescription(conn, cveDescription, vuln_id);
			}
			
			if(updateVDO) {
				rs = updateVulnerabilityVDO(conn, vdoUpdate, cve_id);
			}
			
			if(updateCVSS) {
				rs = updateVulnerabilityCVSS(conn, cvssUpdate, cve_id);
			}
			
			if(updateAffRel) {
				rs = removeProductsFromVulnerability(conn, productsToRemove, cve_id);
			}
			
			rs = atomicUpdateVulnerability(conn, status_id, vuln_id, userID, cve_id, updateInfo);
			
			conn.close();
			conn.commit();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}			
		
		return -1;
		
	}
	
}