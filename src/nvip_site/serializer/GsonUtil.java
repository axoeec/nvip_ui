package nvip_site.serializer;

import java.util.Collection;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

public class GsonUtil {
	public static JsonArray toJsonArray(Gson gson, Collection c, Class clazz) {
    	JsonElement element = gson.toJsonTree(c, clazz);

    	if (!element.isJsonArray() ) {
    	// fail appropriately
    	    //throw new SomeException();
    		return null;
    	}

    	return element.getAsJsonArray();
    }
}
