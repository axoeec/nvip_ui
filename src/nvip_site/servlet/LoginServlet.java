package nvip_site.servlet;

import java.io.BufferedReader;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

import com.google.gson.Gson;

import nvip_site.dao.UserDAO;
import nvip_site.model.User;




@WebServlet("/loginServlet")

public class LoginServlet extends HttpServlet {
	private static final Logger logger = LogManager.getLogger(LoginServlet.class);
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		handleRequest(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		handleRequestPost(req, resp);
	}
	
	public void handleRequestPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException {
		
		boolean createUser = Boolean.parseBoolean(req.getParameter("createUser"));
		
		if (createUser) {

			StringBuilder stringBuilder = new StringBuilder();
            BufferedReader bufferedReader = null;
    	    
    		try {
    			bufferedReader = req.getReader();
    			String line;
    	        while ((line = bufferedReader.readLine()) != null) {
    	        	stringBuilder.append(line);
    	        	stringBuilder.append(System.lineSeparator());
    	        }
    		} catch (IOException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
            
            
    		String dataString = stringBuilder.toString();    		
    		if (dataString==null) {
    			try {
    				resp.setStatus(500);
        			resp.setContentType("text/html");
    				resp.setCharacterEncoding("UTF-8");
    				resp.getWriter().write("Somthing is wrong!");
        			return;
    			}
    			catch (IOException e) {
    				logger.error(e.toString());
    			}
    		}
    		
    		JSONObject userData = new JSONObject(dataString);
    		
    		String userName = userData.getString("username").toLowerCase();
    		String password = userData.getString("password");
    		String fname = userData.getString("fname");
    		String lname = userData.getString("lname");
    		String email = userData.getString("email");
    		
    		User user = new User(null, userName, fname, lname, email, 2);
    		
    		int rs = UserDAO.createUser(user, password);
    		
    		if (rs==-2) {
    			try {
    				resp.setStatus(409);
        			resp.setContentType("text/html");
    				resp.setCharacterEncoding("UTF-8");
    				resp.getWriter().write("User already exists!");
        			return;
    			}
    			catch (IOException e) {
    				logger.error(e.toString());
    			}
    		} 
    		
    		if (rs==-1) {
    			try {
    				resp.setStatus(500);
        			resp.setContentType("text/html");
    				resp.setCharacterEncoding("UTF-8");
    				resp.getWriter().write("Somthing is wrong!");
        			return;
    			}
    			catch (IOException e) {
    				logger.error(e.toString());
    			}
    			
    		}
    		
    		logger.info("Nvip UI user created login for {}", userName);

		}
	}

	public void handleRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException {
		String userName = req.getParameter("userName") == null ? "" : req.getParameter("userName");
		String passwordHash = req.getParameter("passwordHash") == null ? "" : req.getParameter("passwordHash");
		String jObj = null;
		JSONObject map = null;

		logger.info("Nvip UI user login for {}", userName);

		if (userName != null && passwordHash != null) {
			User user = null;

			user = UserDAO.login(userName, passwordHash);

			// System.out.println("Login Successful?: " + success);

			if (user!=null) {
//				map = new JSONObject();
//				map.put("user", user);
				jObj = new Gson().toJson(user);
			}
			else {
				try {
					resp.setStatus(404);
					resp.setContentType("text/html");
					resp.setCharacterEncoding("UTF-8");
					resp.getWriter().write("Login or Password is incorrect!");
				} catch (IOException e) {
					logger.error(e.toString());
				}
				
				return;
			}
		}

		

		try {
			resp.setContentType("text/html");
			resp.setCharacterEncoding("UTF-8");
			resp.getWriter().write(jObj);
		} catch (IOException e) {
			logger.error(e.toString());
		}
	}
}
