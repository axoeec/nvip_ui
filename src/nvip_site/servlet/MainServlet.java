package nvip_site.servlet;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

import com.google.gson.Gson;

import nvip_site.dao.MainDAO;

@WebServlet("/mainServlet")

public class MainServlet extends HttpServlet {
	private static final Logger logger = LogManager.getLogger(MainServlet.class);
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		handleRequest(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		handleRequest(req, resp);
	}

	public void handleRequest(HttpServletRequest req, HttpServletResponse resp)  {
		String countGraphs = req.getParameter("countGraphs") == null ? "" : req.getParameter("countGraphs");
		String jObj = null;
		JSONObject map = null;

		logger.info("MainServlet handleRequest() invoked with params: {}", countGraphs);

		if (countGraphs != null) {
			Map<String, Integer> mainPageCounts = MainDAO.getMainPageCounts();

			if (mainPageCounts != null) {
				map = new JSONObject();
				map.put("mainPageCounts", mainPageCounts);
			}
		}

		jObj = new Gson().toJson(map);

		try {
			resp.setContentType("text/html");
			resp.setCharacterEncoding("UTF-8");
			resp.getWriter().write(jObj);
		} catch (IOException e) {
			logger.error(e.toString());
		}
	}
}
