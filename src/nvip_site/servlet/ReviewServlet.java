package nvip_site.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import nvip_site.dao.LocalDateSerializer;
import nvip_site.dao.ReviewDAO;
import nvip_site.dao.SearchDAO;
import nvip_site.dao.UserDAO;
import nvip_site.model.CVSSupdate;
import nvip_site.model.User;
import nvip_site.model.VDOupdateInfo;
import nvip_site.model.Vulnerability;
import nvip_site.model.VulnerabilityDetails;
import nvip_site.model.VulnerabilityForReviewList;
import nvip_site.serializer.GsonUtil;
 
@WebServlet("/reviewServlet")

public class ReviewServlet extends HttpServlet {
	
	private static final Logger logger = LogManager.getLogger(LoginServlet.class);
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		handleRequestGet(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		handleRequestPost(req, resp);
	}
	
	public void handleRequestPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException {
		
		GsonBuilder gsonBuilder = new GsonBuilder();
    	gsonBuilder.registerTypeAdapter(LocalDate.class, new LocalDateSerializer());
    	Gson gson = gsonBuilder.setPrettyPrinting().create();
    	
    	
		
    	boolean complexUpdate = Boolean.parseBoolean(req.getParameter("complexUpdate"));
    	boolean atomicUpdate = Boolean.parseBoolean(req.getParameter("atomicUpdate"));
    	boolean updateDailyTable = Boolean.parseBoolean(req.getParameter("updateDailyTable"));
    	
    	String userName = req.getParameter("username");
    	String token = req.getParameter("token");
    	
    	if (userName == null || token == null) {
    		try {
				resp.setStatus(401);
    			resp.setContentType("text/html");
				resp.setCharacterEncoding("UTF-8");
				resp.getWriter().write("Unauthorized user!");
    			return;
			}
			catch (IOException e) {
				logger.error(e.toString());
			}
    	}
    	
    	User user = UserDAO.getRoleIDandExpirationDate(userName, token);
    	
    	if (user == null || user.getRoleId()!=1) {
    		try {
				resp.setStatus(401);
    			resp.setContentType("text/html");
				resp.setCharacterEncoding("UTF-8");
				resp.getWriter().write("Unauthorized user!");
    			return;
			}
			catch (IOException e) {
				logger.error(e.toString());
			}
    	}
    	
    	
    	if(atomicUpdate) {
    		int statusID = Integer.parseInt(req.getParameter("statusID"));
    		int userID = user.getUserID();
    		String cveID = req.getParameter("cveID");
    		int vulnID = Integer.parseInt(req.getParameter("vulnID"));
//    		String description = req.getParameter("description");
    		String info = req.getParameter("info");
       		ReviewDAO.atomicUpdateVulnerability(statusID, vulnID, userID, cveID, info);

    	}
    	else if(complexUpdate) {
    		       	
        	boolean updateDescription = Boolean.parseBoolean(req.getParameter("updateDescription"));
    		boolean updateVDO = Boolean.parseBoolean(req.getParameter("updateVDO"));
    		boolean updateCVSS = Boolean.parseBoolean(req.getParameter("updateCVSS"));
    		boolean updateAffRel = Boolean.parseBoolean(req.getParameter("updateAffRel"));
    		
    		int statusID = Integer.parseInt(req.getParameter("statusID"));
    		int userID = user.getUserID();	
    		int vulnID = Integer.parseInt(req.getParameter("vulnID"));
    		String cveID = req.getParameter("cveID");
    		
    		StringBuilder stringBuilder = new StringBuilder();
            BufferedReader bufferedReader = null;
    	    
    		try {
    			bufferedReader = req.getReader();
    			String line;
    	        while ((line = bufferedReader.readLine()) != null) {
    	        	stringBuilder.append(line);
    	        	stringBuilder.append(System.lineSeparator());
    	        }
    		} catch (IOException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
            
            
    		String dataString = stringBuilder.toString();    		
    		if (dataString==null)
    			return;
    		
    		JSONObject dataJSON = new JSONObject(dataString);
    		
//    		String descriptionToUpdate = req.getParameter("descriptionToUpdate");
    		String descriptionToUpdate = dataJSON.getString("descriptionToUpdate");
    		
    		
//    		String cveDescription = req.getParameter("description");
    		String cveDescription = null;
    		VDOupdateInfo vdoUpdate = null;
    		CVSSupdate cvssUpdate = null;
    		int[] productsToRemove = null;
    		
    		if (updateDescription) {
    			cveDescription = dataJSON.getString("description");
    		}
    		
    		if (updateVDO) {
    			vdoUpdate = new VDOupdateInfo(dataJSON.getJSONObject("vdoUpdates"));
    		}
    		
    		if (updateCVSS) {
    			cvssUpdate = new CVSSupdate(dataJSON.getJSONObject("cvss"));
    		}
    		
    		if (updateAffRel) {
    			JSONArray jsonArray = dataJSON.getJSONArray("prodToRemove");
    			productsToRemove = new int[jsonArray.length()];
    			for(int i=0; i<jsonArray.length(); i++) {
    				productsToRemove[i] = jsonArray.getInt(i);
    			}
    		}
    		
    		ReviewDAO.complexUpdate(updateDescription, updateVDO, updateCVSS, updateAffRel, statusID, vulnID, userID, cveID, descriptionToUpdate, cveDescription, vdoUpdate, cvssUpdate, productsToRemove);
    		
//    		System.out.println("VulnID: " + vulnID + " || Description: " + cveDescription);
    	}
    	else if(updateDailyTable) {
    		int out = ReviewDAO.updateDailyVulnerability(3);
    		
    		try {
            	resp.setContentType("text/html");
                resp.setCharacterEncoding("UTF-8");
    			resp.getWriter().write(Integer.toString(out));
    		} catch (IOException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}	
    	}
	
	}
	
	public void handleRequestGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException {
		GsonBuilder gsonBuilder = new GsonBuilder();
    	gsonBuilder.registerTypeAdapter(LocalDate.class, new LocalDateSerializer());
    	Gson gson = gsonBuilder.setPrettyPrinting().create();
    	
    	String userName = req.getParameter("username");
    	String token = req.getParameter("token");
    	
    	if (userName == null || token == null) {
    		try {
				resp.setStatus(401);
    			resp.setContentType("text/html");
				resp.setCharacterEncoding("UTF-8");
				resp.getWriter().write("Unauthorized user!");
    			return;
			}
			catch (IOException e) {
				logger.error(e.toString());
			}
    	}
    	
    	User user = UserDAO.getRoleIDandExpirationDate(userName, token);
    	
    	if (user == null || user.getRoleId()!=1) {
    		try {
				resp.setStatus(401);
    			resp.setContentType("text/html");
				resp.setCharacterEncoding("UTF-8");
				resp.getWriter().write("Unauthorized user!");
    			return;
			}
			catch (IOException e) {
				logger.error(e.toString());
			}
    	}
    	
//    	boolean getDetails = Boolean.parseBoolean(req.getParameter("getDetails"));
    	String cveID = req.getParameter("cveID");
    	
    	String jObj = null;
    	
    	if(cveID!=null) {
    		VulnerabilityDetails vulnDetails = ReviewDAO.getVulnerabilityDetails(cveID);
    		jObj = gson.toJson(vulnDetails);
    	}
    	else {
    		LocalDate searchDate = req.getParameter("searchDate") == null ? null : LocalDate.parse(req.getParameter("searchDate"));
    		  		
    		boolean crawled = req.getParameter("crawled") == null ? false : Boolean.parseBoolean(req.getParameter("crawled"));
    		boolean rejected = req.getParameter("rejected") == null ? false : Boolean.parseBoolean(req.getParameter("rejected"));
    		boolean accepted = req.getParameter("accepted") == null ? false : Boolean.parseBoolean(req.getParameter("accepted"));
    		boolean reviewed = req.getParameter("reviewed") == null ? false : Boolean.parseBoolean(req.getParameter("reviewed"));
    		
//        	List<Vulnerability> searchResults = ReviewDAO.getSearchResults(searchDate);
        	List<VulnerabilityForReviewList> searchResults = ReviewDAO.getSearchResults(searchDate, crawled, rejected, accepted, reviewed);

    		int totalCount = searchResults.size();
    		
    		// Preferred implementation but searchResults fails to get passed for some reason
    		/*
    		map = new JsonObject();
    		map.add("searchResults", GsonUtil.toJsonArray(gson, searchResults.get(totalCount), List.class));
    		map.addProperty("totalCount", totalCount);
    		*/
    		
    		// Places the total count at the end of the array of vulnerabilities. Count is popped off the end of the array in the controller
    		JsonArray arr = GsonUtil.toJsonArray(gson, searchResults, List.class);
    		arr.add(totalCount);
    		jObj = gson.toJson(arr);
    	}
		   	    
        try {
        	resp.setContentType("text/html");
            resp.setCharacterEncoding("UTF-8");
			resp.getWriter().write(jObj);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
}