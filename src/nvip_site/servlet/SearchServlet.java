package nvip_site.servlet;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import nvip_site.dao.LocalDateSerializer;
import nvip_site.dao.SearchDAO;
import nvip_site.model.Vulnerability;
import nvip_site.serializer.GsonUtil;

@WebServlet("/searchServlet")

public class SearchServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(SearchServlet.class);

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		handleRequest(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		handleRequest(req, resp);
	}

	public void handleRequest(HttpServletRequest req, HttpServletResponse resp) {
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(LocalDate.class, new LocalDateSerializer());
		Gson gson = gsonBuilder.setPrettyPrinting().create();

		boolean searchInfo = Boolean.parseBoolean(req.getParameter("searchInfo"));

		JsonObject map = null;

		// Section for Search Form info. Used when Search Form is initialized
		if (searchInfo) {
			Map<String, Map<String, String[]>> searchMap = SearchDAO.getSearchInfo();
			map = new JsonObject();

			String jObj = gson.toJson(searchMap);

			try {
				resp.setContentType("text/html");
				resp.setCharacterEncoding("UTF-8");
				resp.getWriter().write(jObj);
			} catch (IOException e) {
				logger.error(e.toString());
			}

			return; // End the method
		}

		int vulnId = req.getParameter("vulnId") == null ? 0 : Integer.parseInt(req.getParameter("vulnId"));
		String keyword = req.getParameter("keyword") == null ? null : req.getParameter("keyword");
		LocalDate startDate = req.getParameter("startDate") == null ? null : LocalDate.parse(req.getParameter("startDate"));
		LocalDate endDate = req.getParameter("endDate") == null ? null : LocalDate.parse(req.getParameter("endDate"));
		String cvssScores = req.getParameter("cvssScores") == null ? null : req.getParameter("cvssScores");
		String[] inSite = req.getParameterValues("inSite") == null ? null : req.getParameterValues("inSite");
		String[] vdoNounGroups = req.getParameterValues("vdoNounGroups") == null ? null : req.getParameterValues("vdoNounGroups");
		String[] vdoLabels = req.getParameterValues("vdoLabels") == null ? null : req.getParameterValues("vdoLabels");
		int limitCount = req.getParameter("limitCount") == null ? 0 : Integer.parseInt(req.getParameter("limitCount"));
		boolean isBefore = req.getParameter("isBefore") == null ? false : Boolean.parseBoolean(req.getParameter("isBefore"));

		// If there are no selected parameters, do not query the database
		if (keyword != null || startDate != null || endDate != null || cvssScores != null || inSite != null || vdoNounGroups != null || vdoLabels != null) {

			Map<Integer, List<Vulnerability>> searchResults = SearchDAO.getSearchResults(vulnId, keyword, startDate, endDate, cvssScores, vdoNounGroups, vdoLabels, inSite[0], inSite[1], limitCount, isBefore);

			int totalCount = searchResults.entrySet().stream().findFirst().get().getKey();

			// Preferred implementation but searchResults fails to get passed for some
			// reason
			/*
			 * map = new JsonObject(); map.add("searchResults", GsonUtil.toJsonArray(gson,
			 * searchResults.get(totalCount), List.class)); map.addProperty("totalCount",
			 * totalCount);
			 */

			// Places the total count at the end of the array of vulnerabilities. Count is
			// popped off the end of the array in the controller
			JsonArray arr = GsonUtil.toJsonArray(gson, searchResults.get(totalCount), List.class);
			arr.add(totalCount);
			String jObj = gson.toJson(arr);

			try {
				resp.setContentType("text/html");
				resp.setCharacterEncoding("UTF-8");
				resp.getWriter().write(jObj);
			} catch (IOException e) {
				logger.error(e.toString());
			}
		} else {
			// Default search for vulnerabilities
			try {
				resp.setContentType("text/html");
				resp.setCharacterEncoding("UTF-8");
				resp.getWriter().write("");
			} catch (IOException e) {
				logger.error(e.toString());
			}
		}

	}
}