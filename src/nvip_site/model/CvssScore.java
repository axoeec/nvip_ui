package nvip_site.model;

public class CvssScore {
	private String cveId = null;
	private String baseSeverity = null;
	private double severityConfidence = 0;
	private String impactScore = null;
	private double impactConfidence = 0;

	public CvssScore(String cveId, String baseSeverity, double severityConfidence, String impactScore, double impactConfidence) {
		this.cveId = cveId;
		this.baseSeverity = baseSeverity;
		this.severityConfidence = severityConfidence;
		this.impactScore = impactScore;
		this.impactConfidence = impactConfidence;
	}

	public String getCveId() {
		return cveId;
	}

	public void setCveId(String cveId) {
		this.cveId = cveId;
	}

	public String getBaseSeverity() {
		return baseSeverity;
	}

	public void setBaseSeverity(String baseSeverity) {
		this.baseSeverity = baseSeverity;
	}

	public double getSeverityConfidence() {
		return severityConfidence;
	}

	public void setSeverityConfidence(double severityConfidence) {
		this.severityConfidence = severityConfidence;
	}

	public String getImpactScore() {
		return impactScore;
	}

	public void setImpactScore(String impactScore) {
		this.impactScore = impactScore;
	}

	public double getImpactConfidence() {
		return impactConfidence;
	}

	public void setImpactConfidence(double impactConfidence) {
		this.impactConfidence = impactConfidence;
	}

	@Override
	public String toString() {
		return "CvssScore [cveId=" + cveId + ", baseSeverity=" + baseSeverity + ", severityConfidence="
				+ severityConfidence + ", impactScore=" + impactScore + ", impactConfidence=" + impactConfidence + "]";
	}
}
