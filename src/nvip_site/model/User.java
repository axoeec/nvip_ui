package nvip_site.model;

import java.time.LocalDateTime;

public class User {

	private int userID;
	private String token;
	private String userName;
	private String firstName;
	private String lastName;
	private String email;
	private String passwordHash;
	private int roleId;
	private LocalDateTime expirationDate;
	
	public User() {
		
	}
	
	public User(String token, String userName, String firstName, String lastName, String email, int roleId) {
		super();
		this.token = token;
		this.userName = userName;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.roleId = roleId;
	}
	
	

	public User(String token, String userName, String firstName, String lastName, String email, String passwordHash,
			int roleId) {
		super();
		this.token = token;
		this.userName = userName;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.passwordHash = passwordHash;
		this.roleId = roleId;
	}
	
	

	public User(int userID, String token, String userName, String firstName, String lastName, String email,
			String passwordHash, int roleId, LocalDateTime expirationDate) {
		super();
		this.userID = userID;
		this.token = token;
		this.userName = userName;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.passwordHash = passwordHash;
		this.roleId = roleId;
		this.expirationDate = expirationDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setName(String userName) {
		this.userName = userName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}
	
	

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPasswordHash() {
		return passwordHash;
	}

	public void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash;
	}

	public int getUserID() {
		return userID;
	}

	public void setUserID(int userID) {
		this.userID = userID;
	}

	public LocalDateTime getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(LocalDateTime expirationDate) {
		this.expirationDate = expirationDate;
	}

	@Override
	public String toString() {
		return "User [userID=" + userID + ", token=" + token + ", userName=" + userName + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", email=" + email + ", passwordHash=" + passwordHash + ", roleId="
				+ roleId + ", expirationDate=" + expirationDate + "]";
	}

}
