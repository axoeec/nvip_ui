package nvip_site.model;

public class VDOupdateRecord {
	
	private int labelID, groupID;
	private double confidence;
	
	public VDOupdateRecord(int labelID, int groupID, double confidence) {
		super();
		this.labelID = labelID;
		this.groupID = groupID;
		this.confidence = confidence;
	}

	public int getLabelID() {
		return labelID;
	}

	public void setLabelID(int labelID) {
		this.labelID = labelID;
	}

	public int getGroupID() {
		return groupID;
	}

	public void setGroupID(int groupID) {
		this.groupID = groupID;
	}

	public double getConfidence() {
		return confidence;
	}

	public void setConfidence(double confidence) {
		this.confidence = confidence;
	}
	
	
}
