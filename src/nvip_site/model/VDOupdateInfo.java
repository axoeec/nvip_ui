package nvip_site.model;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

public class VDOupdateInfo {
	
	private ArrayList<VDOupdateRecord> vdoRecords = new ArrayList<VDOupdateRecord>();
	
	public VDOupdateInfo(JSONObject vdoUpdateJSON) {
				
		JSONArray vdoUpdates = vdoUpdateJSON.getJSONArray("vdoLabels");
		
		for (int i=0; i<vdoUpdates.length(); i++) {
			JSONObject vdoRecordJSON = vdoUpdates.getJSONObject(i);
			vdoRecords.add(new VDOupdateRecord(vdoRecordJSON.getInt("labelID"), vdoRecordJSON.getInt("groupID"), vdoRecordJSON.getDouble("confidence")));
		}
	}

	public ArrayList<VDOupdateRecord> getVdoRecords() {
		return vdoRecords;
	}
	
	

}
