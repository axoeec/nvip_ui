package nvip_site.model;

public class Product {
    private String domain;
    private String cpe;
    private int productId;
    private String releaseDate;
    private String version;
    
    public Product(int productId, String cpe, String domain, String releaseDate, String version) {
    	this.productId = productId;
    	this.cpe = cpe;
    	this.domain = domain;
        this.releaseDate = releaseDate;
        this.version = version;
    }

    public Product(String domain, String cpe) {
        this.productId = 0;
        this.domain = domain;
        this.cpe = cpe;
    }

    public String getDomain() {
        return domain;
    }
    public String getCpe() {
        return cpe;
    }

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public String getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	@Override
    public String toString() {
        return domain + ": " + cpe;
    }
}
