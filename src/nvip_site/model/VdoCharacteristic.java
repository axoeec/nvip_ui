package nvip_site.model;

public class VdoCharacteristic {
	private String cveId = null;
	private String vdoLabel = null;
	private double vdoConfidence = 0;
	private String vdoNounGroup = null;

	public VdoCharacteristic(String cveId, String vdoLabel, double vdoConfidence, String vdoNounGroup) {
		super();
		this.cveId = cveId;
		this.vdoLabel = vdoLabel;
		this.vdoConfidence = vdoConfidence;
		this.vdoNounGroup = vdoNounGroup;
	}

	public String getCveId() {
		return cveId;
	}

	public void setCveId(String cveId) {
		this.cveId = cveId;
	}

	public String getVdoLabel() {
		return vdoLabel;
	}

	public void setVdoLabel(String vdoLabel) {
		this.vdoLabel = vdoLabel;
	}

	public double getVdoConfidence() {
		return vdoConfidence;
	}

	public void setVdoConfidence(double vdoConfidence) {
		this.vdoConfidence = vdoConfidence;
	}
	
	public String getVdoNounGroup() {
		return vdoNounGroup;
	}

	public void setVdoNounGroup(String vdoNounGroup) {
		this.vdoNounGroup = vdoNounGroup;
	}

	@Override
	public String toString() {
		return "VdoCharacteristic [cveId=" + cveId + ", vdoLabel=" + vdoLabel + ", vdoConfidence=" + vdoConfidence + "]";
	}
}
