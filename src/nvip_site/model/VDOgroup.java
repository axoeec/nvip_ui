package nvip_site.model;

import java.util.HashMap;

public class VDOgroup {
	
	private String vdoGroupName;
	private HashMap<String, String> vdoLabel = new HashMap<String, String>();
	
	public VDOgroup(String vdoGroupName, String vdoLabel, String vdoConf) {
		super();
		this.vdoGroupName = vdoGroupName;
		this.vdoLabel.put(vdoLabel, vdoConf);
	}

	public String getVdoGroupName() {
		return vdoGroupName;
	}

	public void setVdoGroupName(String vdoGroupName) {
		this.vdoGroupName = vdoGroupName;
	}

	public HashMap<String, String> getVdoLabel() {
		return vdoLabel;
	}

	public void setVdoLabel(HashMap<String, String> vdoLabel) {
		this.vdoLabel = vdoLabel;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((vdoGroupName == null) ? 0 : vdoGroupName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VDOgroup other = (VDOgroup) obj;
		if (vdoGroupName == null) {
			if (other.vdoGroupName != null)
				return false;
		} else if (!vdoGroupName.equals(other.vdoGroupName))
			return false;
		return true;
	}
	
	

}
