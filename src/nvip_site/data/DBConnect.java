package nvip_site.data;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class DBConnect {
	private static String dirPath = "C:\\Users\\Steven\\Documents\\GitHub\\nvip\\output\\";
	
	public static Connection getConnection() throws SQLException {

		try {
			Connection conn = null;

			if(getDatabaseType() == "MySQL") {
				Class.forName("com.mysql.cj.jdbc.Driver");
				Context ctx = new InitialContext();
				DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/nvip_db_mysql");
				
				conn = ds.getConnection();
			}
			else if(getDatabaseType() == "SQLite") {
				/* Config file DB Connection */
				Context ctx = new InitialContext();
				DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/nvip_db");
				conn = ds.getConnection();
	
				// End Config File DB Connection
	
				/*
				 * Local Path DB Connecton Class.forName("org.sqlite.JDBC"); // db parameters
				 * String url = "jdbc:sqlite:" + dirPath + "nvip.db"; // create a connection to
				 * the database
				 * 
				 * conn = DriverManager.getConnection(url);
				 */
				// End Local Path DB Connection
	
				// System.out.println("Connection to SQLite has been established.");
			}
			return conn;
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	public static String getDatabaseType() {
		return "MySQL";
		//return "SQLite";
	}
	
	public static void main(String[] args) {}
}