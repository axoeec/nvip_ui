app.controller('MainController', [ '$scope', '$http', '$routeParams', function($scope, $http, $routeParams) {
     
    $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded; charset=utf-8";
    
    $scope.init = function () {
    	$scope.toggleLoadingScreen(true, "nvip-daily-graph-box");
    	$scope.countGraphs();
    };
    
    /** Controller Functions **/
    $scope.countGraphs = function () {
    	$http({
            url : 'mainServlet',
            method : "GET",  
            params : {countGraphs: "all"}       
        }).then(function(response) {
            $scope.countGraphs = {};
            // console.log(response.data);
            
            //$scope.toggleLoadingScreen(false);
            
            angular.forEach(response.data.map.mainPageCounts.map, function(value, key) {
            	$scope.countGraphs[key] = value;
            });
            
            $scope.toggleLoadingScreen(false, "nvip-daily-graph-box");
            // console.log($scope.countGraphs);
        }, function(response) {
            console.log("Failure -> " + response.data);
            //$scope.countGraphs = response.data.map;
        });
	};
    
	$scope.toggleLoadingScreen = function(loading, className){
    	if(className == "nvip-daily-graph-box"){
    		var graphBox = document.getElementsByClassName("nvip-daily-graph-box")[0];
			var loadingScreen = graphBox.getElementsByClassName("nvip-loading-screen")[0];
			var circleGraphs = graphBox.getElementsByClassName("nvip-circle-graph");
    		if(loading){
    			loadingScreen.style.display = 'block';
				angular.forEach(circleGraphs, function(circleGraph, index) {
					circleGraph.style.display = 'none';
				});
    		} 
    		else {
    			loadingScreen.style.display = 'none';
				angular.forEach(circleGraphs, function(circleGraph, index) {
					circleGraph.style.display = 'block';
				});
    		}
    	}
    		
    }
	
	/** Initialization code **/
    $scope.init();
	
	
} ]);