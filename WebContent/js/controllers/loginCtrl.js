app.controller('LoginController', ['$scope', '$rootScope', '$location', 'AUTH_EVENTS', 'AuthService', '$cookieStore', 
	function ($scope, $rootScope, $location, AUTH_EVENTS, AuthService, $cookieStore) {
	// TODO: Clear credentials after login successful OR remove need for scope object when passing data
	$scope.credentials = {
			username: '',
			password: ''
	};
	
	$scope.login = function (credentials) {
		AuthService.login($scope.credentials).then(function (user) {
			$rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
			if(AuthService.isAuthenticated()){
				//$scope.setCurrentUser(user);
				$location.path("");
			}
		  }, 
		  function () {
			  $rootScope.$broadcast(AUTH_EVENTS.loginFailed);
		});
	};
	
	$scope.isLoggedIn = function() {
  		return AuthService.isAuthenticated();
  	}
	
	$scope.getUsername = function() {
		var user = $cookieStore.get('nvip_user');
		
		if (user != null){
			return user.username;
		}
		
		return null;
	}
	
	$scope.getFirstName = function() {
		var user = $cookieStore.get('nvip_user');
		
		if (user != null){
			return user.firstname;
		}
		
		return null;
	}
	
	$scope.logOut = function() {
		$cookieStore.remove('nvip_user');
		window.location.reload();
		return null;
	}
	
	$scope.getUserRole = function() {
		var user = $cookieStore.get('nvip_user');
		
		if (user != null){
			return user.role;
		}
		
		return null;
	}
}]);