var app = angular.module("NVIP", ['ngCookies', 'ngRoute']);

/** #Section Constants **/

app.constant('AUTH_EVENTS', {
    loginSuccess : 'auth-login-success',
    loginFailed : 'auth-login-failed',
    logoutSuccess : 'auth-logout-success',
    sessionTimeout : 'auth-session-timeout',
    notAuthenticated : 'auth-not-authenticated',
    notAuthorized : 'auth-not-authorized'
});

app.constant('USER_ROLES', {
    all : '*',
    admin : 'admin',
    user : 'user'
});

app.constant('VDO_NOUN_GROUPS', {
	all : '*',
	ATTACK_THEATER : 'AttackTheater',
	CONTEXT : 'Context',
	IMPACT_METHOD : 'ImpactMethod',
	LOGICAL_IMPACT : 'LogicalImpact',
	MITIGATION : 'Mitigation'
});

/** #Section Routing **/

app.config(function($routeProvider) {  
    $routeProvider  
  
    // route for the home page
    .when('/', {  
        templateUrl: 'views/main.html',  
        controller: 'MainController'
    })
    .when('/login', {  
        templateUrl: 'views/login.html',  
        controller: 'LoginController'
    })
    .when('/createaccount', {  
        templateUrl: 'views/create_account.html',  
        controller: 'CreateAccountController'
    })
    .when('/daily', {  
        templateUrl: 'views/dailyVulns.html',  
        controller: 'VulnerabilityController'
    })
    .when('/vulnerability/:vulnId', {  
        templateUrl: 'views/vulnerability.html',  
        controller: 'VulnerabilityController'  
    })
    .when('/search', {  
        templateUrl: 'views/search.html',  
        controller: 'SearchController'  
    })
    .when('/review', {  
        templateUrl: 'views/review.html',  
        controller: 'ReviewController'  
    })
    .when('/test', {  
        templateUrl: 'views/test.html',  
        controller: 'VulnerabilityController'  
    })
    .otherwise({
        redirectTo: '/'
    });
  
}); 

app.run(['$rootScope', '$cookies', '$cookieStore', '$location', 'AUTH_EVENTS', 'AuthService',
	function ($rootScope, $cookies, $cookieStore, $location, AUTH_EVENTS, AuthService) {
		// keep user logged in after page refresh
        /*$rootScope.globals = $cookieStore.get('globals') || {};
        if ($rootScope.globals.currentUser) {
            $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
        }*/
        $rootScope.$on('$locationChangeStart', function (event, next, current) {
        	var authorizedRoles = next;
        	
        	if(AuthService.isAuthenticated()) {
        		// Do nothing, let allow them to access full-site	
        		//console.log("Are authenticated");
        	}
        	else {
        		// Re-direct to login page since they are not logged in
        		$location.path('login');
        	}
        	
            //if ($location.path() !== '/login' && !$rootScope.globals.currentUser) {
             //   $location.path('/login');
            //}
        });
}]);

/** #Section Services **/

app.factory('AuthService', function ($http, $cookies, $cookieStore, Session) {
	$http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded; charset=utf-8";
	
	var authService = {};
	 
	  authService.login = function (credentials) {
		  // Add Method to encrypt password
		  var password_hash = credentials.password;
		  var request = {
          	method: "GET",
          	params: {'userName': credentials.username, 'passwordHash': password_hash},
          	url: "loginServlet"
          };
		  
		  return $http(request)
		  	.then(function(response) {
		  		if (response.data!=null && response.data!=""){
		  			Session.create(1, response.data.userName, response.data.firstName, response.data.token, response.data.expirationDate);
	            	$cookieStore.put('nvip_user', Session);
		  		}
		  	})
		  	.catch(function(response) {
	            console.log("Failure -> " + response.data);
	        });
	  };
	 
	  authService.isAuthenticated = function () {
		  if($cookieStore.get('nvip_user') != null || $cookieStore.get('nvip_user') != undefined){
			  // May be more involved process to check if session identifier is legitimate
			 return ($cookieStore.get('nvip_user').id != null || $cookieStore.get('user').id != undefined);
		  }
		  
		  return false;
	  };
	 
	  authService.isAuthorized = function (authorizedRoles) {
	    if (!angular.isArray(authorizedRoles)) {
	      authorizedRoles = [authorizedRoles];
	    }
	    return (authService.isAuthenticated() &&
	      authorizedRoles.indexOf(Session.userRole) !== -1);
	  };
	 
	  return authService;
});

app.service('Session', function () {
	this.create = function (sessionId, username, firstname, token, expiration) {
		this.id = sessionId;
		this.username = username;
		this.firstname = firstname;
		this.token = token;
		this.expiration = expiration;
	};
	
	this.destroy = function () {
		this.id = null;
		this.username = null;
		this.firstname = null;
		this.token = null;
		this.expiration = null;
	};
});

/** #Section Controllers **/

app.controller("VulnerabilityController", [ '$scope', '$http', '$routeParams', 'AuthService', 'VDO_NOUN_GROUPS', function($scope, $http, $routeParams, AuthService, VDO_NOUN_GROUPS) {
     
    $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded; charset=utf-8";
    $scope.vulnId = $routeParams.vulnId;
    $scope.vulnSearch = "";
    
    $scope.dailyVulnLimit = []; 
    $scope.vulnLimitIncr = 5;
    $scope.dailyVulnIndex = 0; // 0

    $scope.init = function (id) {
	    $scope.getDailyVulns();
  	};
  	
    $scope.getVulnInfo = function() {
        $http({
            url : 'vulnerabilityServlet',
            method : "GET",  
            params : { vulnId : $scope.vulnId }       
        }).then(function(response) {
            if(response != null) {
                $scope.vuln = response.data;
                $scope.fillVulnTabs($scope.vuln);
            } else {
                console.log(response)
                $scope.vuln = null;
            }
        }, function(response) {
            console.log("Failure -> " + response.data);
            $scope.vuln = null;
        });
    };

    $scope.getDailyVulns = function() {
    	$scope.toggleLoadingScreen(true, "nvip-daily-vuln-ctn");
        $http({
            url : 'vulnerabilityServlet',
            method : "GET",  
            params : {daily: true, dateRange: 10} // 10      
        }).then(function(response) {
            $scope.dailyVulns = [];
            //console.log(response.data);
            
            $scope.toggleLoadingScreen(false, "nvip-daily-vuln-ctn");
            
            angular.forEach(response.data, function(value, key) {
            	$scope.dailyVulnLimit.push($scope.vulnLimitIncr);
                $scope.dailyVulns.push({date: formatDate(value.date), cve_list: value.list});
            });
            //console.log($scope.dailyVulns);
        }, function(response) {
            console.log("Failure -> " + response.data);
            $scope.dailyVulns = response.data;
        });
    };
    
    $scope.searchFocusOut = function(){
        // console.log("Search bar lost focus");
    }
    $scope.searchVulns = function(vulnSearch){
        console.log("Keyup");

        if (vulnSearch.length > 2) {
            $http({
                url : 'vulnerabilityServlet',
                method : "GET",  
                params : {match: "Gentoo"}       
            }).then(function(response) {
                console.log("Ello");
                console.log(response);
            }, function(response) {
                console.log("Failure -> " + response.data);
                $scope.dailyVulns = response.data.map;
            });
        }
    }
    
    $scope.fillVulnTabs = function(vuln) {
    	// Severity Tab
    	var cvssScores = vuln.cvssScoreList;
    	var cvssScoreLabels = [["Base", "Impact"]];
    	
    	angular.forEach(cvssScores, function(cvssScore, key) {
    		if (key == 0) {
    			var severityBar = document.getElementsByClassName("vuln-severity-bar")[0];
    			var impactBar = document.getElementsByClassName("vuln-impact-bar")[0];
    			// Determine block height and color based on severity level
    			if (cvssScore.baseSeverity == "CRITICAL") {
    				severityBar.style.height = "22.5em";
    				severityBar.style.backgroundColor = "#000000";
    				severityBar.children[0].innerText = "9.0";
    			}
    			else if (cvssScore.baseSeverity == "HIGH"){
    				severityBar.style.height = "17.5em";
    				severityBar.style.backgroundColor = "#d9534f";
    				severityBar.children[0].innerText = "7.0";
    			}
    			else if (cvssScore.baseSeverity == "MEDIUM") {
    				severityBar.style.height = "12.5em";
    				severityBar.style.backgroundColor = "#f2cc0c";
    				severityBar.children[0].innerText = "5.0";
    			}
    			else if (cvssScore.baseSeverity == "LOW") {
    				severityBar.style.height = "7.5em";
    				severityBar.style.backgroundColor = "#7ebe18";
    				severityBar.children[0].innerText = "3.0";
    			}
    			
    			var impactBarHeight = getBarHeight(cvssScore.impactScore);
    			impactBar.style.height = impactBarHeight + "em";
    			impactBar.children[0].innerText = cvssScore.impactScore;
    			impactBar.style.backgroundColor = "#7ebe18";
    			console.log(impactBarHeight);
    		}
    	});
    	
    	// Characteristics Tabs
    	var vdoList = vuln.vdoList;
    	var vdoGraph = document.getElementsByClassName("vuln-characteristics-graph")[0];
		var vdoBar = null;
    	var vdoBarText = null;
		
    	vdoList.sort(function(vdo1, vdo2){
    		var nounGroup1 = vdo1.vdoNounGroup;
    		var nounGroup2 = vdo2.vdoNounGroup;
    		
    		var nounGroupCmp = nounGroup1.localeCompare(nounGroup2);
    		
    		if(nounGroupCmp == 0){
    			
    			if (vdo1.vdoConfidence == vdo2.vdoConfidence){
	    			var label1 = vdo1.vdoLabel;
	    			var label2 = vdo2.vdoLabel;
	    			
	    			return label1.localeCompare(label2);
    			}
    			else if(vdo1.vdoConfidence > vdo2.vdoConfidence){
    				return -1;
    			}
    			else{
    				return 1;
    			}
    		}
    		else {
    			return nounGroupCmp;
    		}
    	});
    	
    	angular.forEach(vdoList, function(vdo, key) {
    		vdoBar = document.createElement("DIV");
			vdoBar.classList.add("vuln-characteristics-bar");
			
			vdoBarText = document.createElement("P");
			vdoBarText.innerText = vdo.vdoNounGroup + " : " + vdo.vdoLabel;
			vdoBar.appendChild(vdoBarText);
			
			vdoBarText = document.createElement("P");
			vdoBarText.innerText = (parseFloat(vdo.vdoConfidence)*100).toFixed(2)+"%";
			vdoBar.appendChild(vdoBarText);
			
			vdoGraph.appendChild(vdoBar);
    	});
    }

    function getBarHeight(value) {
    	var barHeight = "2.5";
    	
    	if (isNaN(value))
    		return 0;
    		
    	return barHeight * value;
    }
    
    $scope.showLess = function(panelIndex){
    	var newLimit = $scope.dailyVulnLimit[panelIndex] - $scope.vulnLimitIncr;

    	if(newLimit < $scope.vulnLimitIncr) {
    		$scope.dailyVulnLimit[panelIndex] = $scope.vulnLimitIncr;
    	}
    	else {
    		$scope.dailyVulnLimit[panelIndex] = newLimit;
    	}
    }
    
    $scope.showMore = function(panelIndex){
    	$scope.dailyVulnLimit[panelIndex] = $scope.dailyVulnLimit[panelIndex] + $scope.vulnLimitIncr;
    }
    
    $scope.toggleLoadingScreen = function(loading, className){
    	// Show the loading screen
    	
    	if(className == "nvip-daily-vuln-ctn"){
    		var dailyVulnCtn = document.getElementsByClassName("nvip-daily-vuln-ctn")[0];
        	var loadingScreen = dailyVulnCtn.getElementsByClassName("nvip-loading-screen")[0];
        	//var panelCtns = dailyVulnCtn.getElementsByClassName("nvip-daily-vuln-panel-ctn");
        	
    		if(loading){
    			loadingScreen.style.display = 'block';
    		} 
    		else {
    			loadingScreen.style.display = 'none';
    		}
    	}
    		
    }
    
    // Main Page functions
    $scope.incrementDailyVulnDay = function(incr){
    	if($scope.dailyVulnIndex + incr < 0){
    		return;
    	}
    	else if ($scope.dailyVulnIndex + incr >= $scope.dailyVulns.length) {
    		return;
    	}
    	
    	$scope.dailyVulnIndex = $scope.dailyVulnIndex += incr;
    }
    
} ]);

/** #Section Global Functions **/

function getAncestor(element, className) {
	
	if (element == null) {
		return null;
	}
	
	// If the given element has the desired class, return it instead of looking for
	// an earlier class
	if(element.classList.contains(className)){
		return element;
	}
	
	var parent = element.parentElement; 
	
	while(parent != null){
		if (parent.classList.contains(className)) {
			return parent;
		}
		parent = parent.parentElement;
	}
	
	return null;
}

function getSiblingByClassName(element, className) {
	if (element == null){
		return null;
	}
	
	var sibling = element.nextSibling;
	
	while(sibling){
		if(sibling.nodeType === 1 && sibling != element){
			if (sibling.classList.contains(className)){
				return sibling;
			}
		}
		
		sibling = sibling.nextSibling;
	}
	
	return null;
}

function openTab(tabClass) {
        var x = document.getElementsByClassName("vuln-tab");
        var tabButtons = document.getElementsByClassName("vuln-tab-button");
        for (i = 0; i < x.length; i++) {
            if (x[i].classList.contains(tabClass)){
                x[i].style.display = "block";
                tabButtons[i].style.backgroundColor = "#ffffff";
            }
            else {
                x[i].style.display = "none";
                tabButtons[i].style.backgroundColor = "#f0f0f0";
            }
        }
    }

function formatDate(dateString) {
	var timeZone = "T00:00:00.000-08:00";
	var date = new Date(dateString + timeZone);
	var today = new Date();
	
	return date.toDateString();
}