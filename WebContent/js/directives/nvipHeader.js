/**
 * 
 */
app.directive('nvipHeader', function() {
	
	 return {
		 link: function (scope, elem, attrs) {
			
		 },
		 restrict: "E",
		 scope: {
		 },
		 template: 
		 `<div class="nvip-header col-12" ng-controller="LoginController as LoginCtrl">
		 	<div class="nvip-logo-container">
				<a href="#/" class="nvip-logo">
					<img src="images/nvip-simple-logo.jpg" alt="NVIP">
				</a>
			</div>
			<div class="nvip-header-container">
				<div id="userView"  class="nvip-header-links col-10" style="display:none;">
					<div class="nvip-header-link col-3">
						<a>Vulnerabilities</a>
					</div>
					<div class="nvip-header-link col-3">
						<a>Products</a>
					</div>
					<div class="nvip-header-link col-3">
						<a>Daily Stats</a>
					</div>
					<div class="nvip-header-link col-3">
						<a href="#search/">Search</a>
					</div>
				</div>
				<div id="adminView" class="nvip-header-links col-10">
					<div class="nvip-header-link col-13">
						<a>Vulnerabilities</a>
					</div>
					<div class="nvip-header-link col-13">
						<a>Products</a>
					</div>
					<div class="nvip-header-link col-13">
						<a>Daily Stats</a>
					</div>
					<div class="nvip-header-link col-13">
						<a href="#search/">Search</a>
					</div>
					<div class="nvip-header-link col-13">
						<a href="#review/">Review</a>
					</div>
				</div>
				<div class="nvip-header-user-icon-container col-2">
					<a class="nvip-header-user-icon fa fa-sign-out" ng-hide="!isLoggedIn()" aria-hidden="true" ng-click="logOut()" href="#"></a>
					<p class="nvip-user-label" ng-hide="!isLoggedIn()">Welcome, {{getFirstName()}} </p>
					
				</div>
			</div>
		 </div>`
			 
	 };
});

function switchViewToAdmin() { 
	var userView = document.getElementById("userView"); 
	var adminView = document.getElementById("adminView");
	
	userView.style.display = "none"; 
	adminView.style.display = "flex";
	adminView.classList.add("nvip-header-links col-10");
	 
}
