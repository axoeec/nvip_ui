/**
 * 
 */
app.directive('nvipFooter', function() {
	 return {
		 link: function (scope, elem, attrs) {
			
		 },
		 restrict: "E",
		 scope: {
		 },
		 template: 
		 `<div class="nvip-footer col-12">
			<p>
				Performer is looking for feedback on its stand-up software vulnerabilities platform and methodology for detecting and reporting software vulnerabilities for software assurance community by establishing a website for obtaining feedback to assist with their research. S&T is funding this research through contract 70RSAT19CB0000020
			</p>
			<div class="nvip-footer-contents">
				<a>About NVIP</a><a>Privacy Policy</a><a>Contact Us</a>
			</div>
			<div class="nvip-footer-social-media">
				<p>Follow us @Twitter @LinkedIn @Facebook</p>
			</div>
		</div>`
	 };
});